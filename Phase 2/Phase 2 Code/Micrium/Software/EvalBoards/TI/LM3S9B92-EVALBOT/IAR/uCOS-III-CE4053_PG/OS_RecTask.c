/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include "OS_RecTask.h"

/*
************************************************************************************************************************
*                                                 GLOBAL VARIABLE
************************************************************************************************************************
*/


//Global Variable

int systemCeiling = 99;

np *list;
int height=1,width=1;
np *ln;

int releaseTime = 0;

//Red Black Tree
//Red Black Tree Node
OS_MEM            redblacktreeNodeMEM;
RedBlackNode      alloMemredblackNode[40];   //1 row for 40 words of RedBlackNode

//Red Black Tree
OS_MEM            redblackTreeMEM;
RedBlackTree      alloMemredblackTree[1];    //1 row for 1 word of RedBlackTree

//Red Black recursive List reference
RedBlackTree *redBlackTree = (RedBlackTree *)0;


//Middle_TCB
OS_MEM            OS_Middle_MEM;
Middle_TCB       alloMemOS_Middle_TCB[40];


//Skip List
OS_MEM            OS_TCB_MEM;
OS_TCB            OS_TCB_STORAGE[50];

//SkipList Mem Block
OS_MEM            SkipList_MEM;
np                SkipList_STORAGE[50];

//AVL Memory Partition
AVL_node        *AVL_root;
OS_MEM          AVL_Partition;
AVL_node        AVL_array[50];

//Stack variable
const int STACK_MAXSIZE = 20;        
int top = -1; 

//Stack
OS_MEM            Stack_MEM;
OS_MUTEX*         Stack_STORAGE[20];

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 Skip List
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void SkipList_init(void)
{
  OS_ERR        err;
  
  OSMemCreate(  (OS_MEM         *)&SkipList_MEM,
                (CPU_CHAR       *)"SkipListPartition",
                (void           *)&SkipList_STORAGE[0],
                (OS_MEM_QTY      ) 50,
                (OS_MEM_SIZE     ) sizeof(np),
                (OS_ERR         *)&err);
  
  OSMemCreate(  (OS_MEM         *)&OS_TCB_MEM,
                (CPU_CHAR       *)"OS_TCBPartition",
                (void           *)&OS_TCB_STORAGE[0],
                (OS_MEM_QTY      ) 50,
                (OS_MEM_SIZE     ) sizeof(OS_TCB),
                (OS_ERR         *)&err);
  
}

// ******************** Testing for skip list ************* //
void insert_sl(OS_TCB *item){
    if(item<=0)return ;
    if(!list){//for the first item
        list=createNode();
        np *newnode=createNode();
        list->right=newnode;
        newnode->left=list;
        newnode->ptr_tcb = item;
        //toss to go upper level
        toss_it(list->right);
        width++;
        
        return;
    }
    //if list is not empty, find the right position
    np *curr=list,*temp;
    while(curr!=(np*)0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item->deadline){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item->deadline)return;
        else {
            curr=curr->down;
            
        }
    }
     
    np *newnode=createNode();
    newnode->ptr_tcb =item;
    if(temp->right==(np*)0){//when added at the right most
        temp->right=newnode;
        newnode->left=temp;
    }
    else{//when added between two nodes
        newnode->left=temp;
        newnode->right=temp->right;
        temp->right->left=newnode;
        temp->right=newnode;      
    }      
    toss_it(newnode);
    width++;
    return ;  
}


// ******************** Testing for skip list ************* //
OS_TCB* search_sl(int item){
    np *curr=list,*temp;
    while(curr!=0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item)
          return curr->right->ptr_tcb;
        else {
            curr=curr->down;
            
        }
    }
      return 0;  
}

// ******************** Testing for skip list ************* //
void delete_sl(int item){
    np *curr=list,*temp;
    int down_count=0;
    while(curr!=(np*)0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item){
            np *nodeTemp=curr->right;
            while(nodeTemp){//go down one by one
                if(nodeTemp->left==((np*)0) && nodeTemp->right==((np*)0)){
                    down_count++;//count the level from uppermost level      
                }
                if(nodeTemp->right){
                    nodeTemp->right->left=nodeTemp->left;
                    nodeTemp->left->right=nodeTemp->right;              
                }
                else{
                    nodeTemp->left->right=(np*)0;
                }
                np *nd=nodeTemp->down;
                
                OS_ERR err;

                OSMemPut( (OS_MEM *)&SkipList_MEM,
                          (void   *)nodeTemp,
                          (OS_ERR *) &err);
                
                nodeTemp=nd;          
            }
            //update the width and height
            width--;
            height=height-down_count;
            return ;
        }
        else {
            curr=curr->down;
         
        }
    }
    return ;
      
}

// ******************** Testing for skip list ************* //
void toss_it(np *x){
    int h=1;
    while(toss_coin()){
        //printf("\nToss Win");
        h++;
        if(h>height){//create a new level
            height=h;
            ln=createNode();
            ln->down=list;
            list->up=ln;
            list=ln;
            //add the node to the new level
            np *newnode=createNode();
            ln->right=newnode;
            newnode->ptr_tcb=x->ptr_tcb;
            newnode->down=x;
            newnode->left=ln;
            x->up=newnode;
            x=newnode;
        }
        else{//add the node to an existing level      
            np *temp=x->left;
            while(temp->up==(np*)0){
                temp=temp->left;
            }
            temp=temp->up;
            np *newnode=createNode();
            newnode->ptr_tcb=x->ptr_tcb;
            newnode->left=temp;
            newnode->down=x;
            temp->right=newnode;
            x->up=newnode;
            x=newnode;
        }  
    }  
}

// ******************** Testing for skip list ************* //
int toss_coin(){
    float t=(float)(rand()%100)/100;
    return t>0.5?1:0;
}

// ******************** Testing for skip list ************* //
np *createNode(){
    OS_ERR      err;
    
    np *newnode =      (np *)OSMemGet((OS_MEM *)&SkipList_MEM,
                                      (OS_ERR *)&err);
    
    newnode->ptr_tcb = (OS_TCB*)OSMemGet((OS_MEM *)&OS_TCB_MEM,
                                         (OS_ERR *)&err);
    
    newnode->ptr_tcb->deadline = -1;
    
    newnode->left = (np*)0;
    
    newnode->down = (np*)0;
    
    newnode->up = (np*)0;
    
    newnode->right = (np*)0;
    
    return newnode;
}

// ******************** Testing for skip list ************* //
OS_TCB* searchSkip()
{
  OS_TCB* temp;
  
  for (int i = 0; i<1; i++)
  {
    switch(i)
    {
      case 0: temp = search_sl(5+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 1: temp = search_sl(7+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 2: temp = search_sl(14+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 3: temp = search_sl(35+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 4: temp = search_sl(60+releaseTime);
              if(temp!= 0)
              {return temp;}
    }
  }
}

/*
************************************************************************************************************************
*                                                 RED BLACK TREE
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void RedBlackTree_init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    //Allocate Memory to Red Black Tree Node
    OSMemCreate((OS_MEM         *)&redblacktreeNodeMEM,
                (CPU_CHAR       *)"RedBlackTreeNodePartition",
                (void           *)&alloMemredblackNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(RedBlackNode),
                (OS_ERR         *)&err);
    
    
    //Allocate Memory to Red Black Tree
    OSMemCreate((OS_MEM         *)&redblackTreeMEM,
                (CPU_CHAR       *)"RedBlackTreePartition",
                (void           *)&alloMemredblackTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(RedBlackTree),
                (OS_ERR         *)&err);
    
     //Allocate Memory to Middle_TCB
    OSMemCreate((OS_MEM         *)&OS_Middle_MEM,
                (CPU_CHAR       *)"Middle_TCB",
                (void           *)&alloMemOS_Middle_TCB[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(Middle_TCB),
                (OS_ERR         *)&err);    
    
    
    //Initialize nodelists
    redBlackTree = (RedBlackTree*)OSMemGet(
                                      (OS_MEM  *)&redblackTreeMEM, 
                                      (OS_ERR  *)&err);
    
    redBlackTree->rootNode = (RedBlackNode*)0;
    redBlackTree->neel =  (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    redBlackTree->neel->color = 1       ;
    
}

//BST insertion
void RedBlackTree_insert(Middle_TCB * _ptr_tcb)
{
  OS_ERR err;
  
  RedBlackNode *y = redBlackTree->neel;
  RedBlackNode *x = redBlackTree->rootNode;
  RedBlackNode *newNode = (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    
  newNode->ptr_tcb = _ptr_tcb;
  
  if(redBlackTree->rootNode != (RedBlackNode*)0)
  {
    while(x != redBlackTree->neel)
    {
      y = x;
      if(newNode->ptr_tcb->timeRemaining < x->ptr_tcb->timeRemaining)
      {
        x = x->left;
      }
      else
      {
        x = x->right;      
      }
    }
  }
  newNode->parent = y;
  
  if(y == redBlackTree->neel)
  {
    redBlackTree->rootNode = newNode;    
  }
  else if(newNode->ptr_tcb->timeRemaining < y->ptr_tcb->timeRemaining)
  {
    y->left = newNode;    
  }
  else
  {
    y->right = newNode;
  }
  
  newNode->left = redBlackTree->neel;
  newNode->right = redBlackTree->neel;
  newNode->color = (CPU_INT08U)0;
    
  RedBlackTree_InsertFix(newNode);
} 

//Red Black Tree deletion
void RedBlackTree_delete(Middle_TCB *_ptr_tcb)
{
 
  OS_ERR err;
  
  //Check if the node is exist
  RedBlackNode *delete_node = search(_ptr_tcb);
  
  if(delete_node == (RedBlackNode*)0)
  {
    return;
  }
  
  //To be deleted and return memory 
  RedBlackNode *y = (RedBlackNode*)0;
  //The child to be deleted
  RedBlackNode *x = (RedBlackNode*)0;

  if(delete_node->left == redBlackTree->neel || delete_node->right == redBlackTree->neel)
  {
    y = delete_node;    
  }
  else
  {
    y = Successor(delete_node);
  }

  if(y->left != redBlackTree->neel)
  {
    x = y->left;
  }
  else
  {
    x = y->right;   
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;    
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
   y->parent->right = x;     
  }
  
  if(y != delete_node)
  {
    delete_node->ptr_tcb = y->ptr_tcb;
  }
  
  if(y->color ==1)
  {
    RedBlackTree_DeleteFix(x);
  }
  
  //Returning memory 
  
  OSMemPut((OS_MEM *)&redblacktreeNodeMEM,
           (void   *)y,
           (OS_ERR *) &err);
  
}

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  
  BSTRecursiveFinding(temp);
}
                 
//Update timeRemaining                 
void RedBlackTree_updateTime(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  OS_ERR err;
  CPU_SR_ALLOC();
  OSSchedLock(&err);
  RedBlackTree_recursiveUpdate(temp);   
  OSSchedUnlock(&err);
}

//Inserting to Skip List
void RedBlackTree_recursiveUpdate(RedBlackNode *curNode)
{
  int preemptionlevel;
  OS_ERR err;
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  curNode->ptr_tcb->timeRemaining--;
    
  if(curNode->ptr_tcb->timeRemaining == (CPU_INT32U)0)
  {         
    
    Middle_TCB *amiddle_tcb = curNode->ptr_tcb;
    
    OS_RecTaskSkipListCreate(
                 (OS_TCB     *) amiddle_tcb->p_tcb, 
                 (CPU_CHAR   *) amiddle_tcb->p_name, 
                 (OS_TASK_PTR ) amiddle_tcb->p_task, 
                 (void       *) amiddle_tcb->p_arg, 
                 (OS_PRIO     ) amiddle_tcb->prio, 
                 (CPU_STK    *) amiddle_tcb->p_stk_base, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_limit, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_size, 
                 (OS_MSG_QTY  ) amiddle_tcb->q_size, 
                 (OS_TICK     ) amiddle_tcb->time_quanta, 
                 (void       *) (CPU_INT32U) amiddle_tcb->p_ext, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
                 (CPU_INT32U) amiddle_tcb->period,
                 (CPU_INT32U) amiddle_tcb->deadline);  
    
    amiddle_tcb->p_tcb->deadline = releaseTime + amiddle_tcb->p_tcb->period;
    
    /*--------- If Task Preemption Level > System Ceiling ------------------- */
    preemptionlevel = amiddle_tcb->prio;
    if(preemptionlevel < systemCeiling)
    {
      /*-------------------- Add to Skip List ----------------------------------*/
      //AVL_insertDeadline(amiddle_tcb->p_tcb);
      insert_sl(amiddle_tcb->p_tcb); 
      //printf("RBT Priority: %d, RBT Deadline: %d \n", amiddle_tcb->p_tcb->Prio, amiddle_tcb->p_tcb->deadline);
    }
    
    /*--------- Else If Task Preemption Level < System Ceiling -------------- */
    else
    {
      /*-------------------- Add to AVL Tree  ----------------------------------*/
      AVL_insertDeadline(amiddle_tcb->p_tcb);
    }
    
    
    amiddle_tcb->timeRemaining = amiddle_tcb->period;
  }
   
  RedBlackTree_recursiveUpdate(curNode->left);
  
  RedBlackTree_recursiveUpdate(curNode->right);   
}

//Initial Release all task to Skip List
void RedBlackTree_initialRelease(RedBlackNode *curNode)
{
  int preemptionlevel;
  
  if(curNode == redBlackTree->neel)
  {
      return;
  }
     
  OS_ERR err;
  OSSchedLock(&err);
  
  Middle_TCB *amiddle_tcb = curNode->ptr_tcb;
    
  OS_RecTaskSkipListCreate(
                 (OS_TCB     *) amiddle_tcb->p_tcb, 
                 (CPU_CHAR   *) amiddle_tcb->p_name, 
                 (OS_TASK_PTR ) amiddle_tcb->p_task, 
                 (void       *) amiddle_tcb->p_arg, 
                 (OS_PRIO     ) amiddle_tcb->prio, 
                 (CPU_STK    *) amiddle_tcb->p_stk_base, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_limit, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_size, 
                 (OS_MSG_QTY  ) amiddle_tcb->q_size, 
                 (OS_TICK     ) amiddle_tcb->time_quanta, 
                 (void       *) (CPU_INT32U) amiddle_tcb->p_ext, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
                 (CPU_INT32U) amiddle_tcb->period,
                 (CPU_INT32U) amiddle_tcb->deadline);  
    
  amiddle_tcb->p_tcb->deadline = releaseTime + amiddle_tcb->p_tcb->period;
    
    /*--------- If Task Preemption Level > System Ceiling ------------------- */
    preemptionlevel = amiddle_tcb->prio;
    if(preemptionlevel < systemCeiling)
    {
      /*-------------------- Add to Skip List ----------------------------------*/
      insert_sl(amiddle_tcb->p_tcb); 
      //printf("If true %d \n", amiddle_tcb->prio);
    }
    
    /*--------- Else If Task Preemption Level < System Ceiling -------------- */
    else
    {
      /*-------------------- Add to AVL Tree  ----------------------------------*/
      AVL_insertDeadline(amiddle_tcb->p_tcb);
      //printf("If false %d \n", amiddle_tcb->prio);
    }
     
  RedBlackTree_initialRelease(curNode->left);
  
  RedBlackTree_initialRelease(curNode->right);   
  
  OSSchedUnlock(&err);
}

//Additional function
RedBlackNode* LeftMost(RedBlackNode* current)
{
  while(current->left != (RedBlackNode*)0)
  {
    current = current->left; 
  }
  return current;
}

RedBlackNode* Successor(RedBlackNode* current)
{
  if(current->right != (RedBlackNode*)0)
  {
    return LeftMost(current->parent);   
  }
  
  RedBlackNode *newNode = current->parent;
  
  while(newNode != (RedBlackNode*)0 && current == newNode->right)
  {
    current = newNode;
    newNode = newNode->parent;    
  }
  return newNode;
}

//Arrange tree after insertion
void RedBlackTree_InsertFix(RedBlackNode *current)
{
  //case 0: if parent is black, no while loop
  
  while(current->parent->color == 0) //if parent is red, go into loop
  {
    //if parent is grandparent's left child
    
    if(current->parent == current->parent->parent->left)
    {
      RedBlackNode *uncle = current->parent->parent->right;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->right)
        {
          current = current->parent;
          RedBlackTree_LeftRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_RightRotate(current->parent->parent);
      }
    }
    //if parent is grandparent's right child
    else
    {
      RedBlackNode *uncle = current->parent->parent->left;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->left)
        {
          current = current->parent;
          RedBlackTree_RightRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_LeftRotate(current->parent->parent);
      }      
    }
  }
  redBlackTree->rootNode->color = 1;
}

//Arrange the tree after deletion
void RedBlackTree_DeleteFix(RedBlackNode *current)
{
  //case 0: if current is red, change it to black 
  //        if current is root, change it to black
  
  while(current != redBlackTree->rootNode && current->color == 1)
  {
    //if current is leftchild
    if(current == current->parent->left)
    {
      RedBlackNode *sibling = current->parent->right;
      
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_LeftRotate(current->parent);
        sibling = current->parent->right;        
      }
      
      //Enter case 2, 3 and 4 where sibling is black
      //case 2: if the two childs of sibling are black
      if(sibling->left->color == 1 && sibling->right->color ==1)
      {
        sibling->color = 0;
        //if current is updated to be root, exit the while loop
        current = current->parent;        
      }
      
      //case 3 and 4: there is only one child of current is black
      else
      {
        //case 3: sibling's right child is black and left child is red
        if(sibling->right->color == 1)
        {
            sibling->left->color = 1;
            sibling->color = 0;
            RedBlackTree_RightRotate(sibling);
            sibling = current->parent->right;
        }
        
        //case 4: sibling's left child is black and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        RedBlackTree_LeftRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }
    }
    //current is right child
    else
    {
      RedBlackNode *sibling = current->parent->left;
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_RightRotate(current->parent);
        sibling = current->parent->left;        
      }
      //Enter case 2, 3 and 4 where sibling is black
      else
      {
        //case 3: sibling's left child is black and right child is red
        if(sibling->left->color == 1)
        {
          sibling->right->color = 1;
          sibling->color = 0;
          RedBlackTree_LeftRotate(sibling);
          sibling = current->parent->left;  
        }
        //case 4: sibling's left child is red and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->left->color = 1;
        RedBlackTree_RightRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }  
    }    
  }
  current->color = 1; 
}
              
//left rotate about the node
void RedBlackTree_LeftRotate(RedBlackNode *x)
{
   RedBlackNode *y = x->right;
   
   x->right = y->left;
   
   if(y->left != redBlackTree->neel)
   {
     y->left->parent = x;
   }
   
   y->parent = x->parent;
   
   if(x->parent == redBlackTree->neel)
   {
     redBlackTree->rootNode = y;
   }
   else if(x == x->parent->left)
   {
     x->parent->left = y;
   }
   else
   {
     x->parent->right = y;     
   }
   
   y -> left = x;
   x-> parent = y;
}
//right rotate about the node
void RedBlackTree_RightRotate(RedBlackNode *y)
{
  RedBlackNode *x = y->left;
  
  y->left = x->right;
  
  if(x->right != redBlackTree->neel)
  {
    x->right->parent = y;
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
    y->parent->right = x;    
  }
  
  x->right  = y;
  y->parent = x;
}

//Recursively print nodes
void  BSTRecursiveFinding(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  printf("%d\n",curNode->ptr_tcb->timeRemaining);
  
  BSTRecursiveFinding(curNode->left);
  
  BSTRecursiveFinding(curNode->right);
}

//Get the avaliable leaf node based on ptr tcb
RedBlackNode *BSTFindSlots(Middle_TCB* _ptr_tcb)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;
  
  RedBlackNode *temp = (RedBlackNode*)0;
  
  //Root is null
  if(root == (RedBlackNode*)0)
  {
    return (RedBlackNode*)0;    
  }
  
  //start from root
  temp = root;
  
  //Break while loop if the current temp is the leaf node
  while( (temp->left  != redBlackTree->neel) &&
         (temp->right != redBlackTree->neel))
  {
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
  }  
  
  return temp;
}

RedBlackNode* search(Middle_TCB *_ptr_tcb)
{
  RedBlackNode *temp = redBlackTree->rootNode;
  
  RedBlackNode *toBeReturned = (RedBlackNode*)0;
  
  while(temp != redBlackTree->neel)
  {
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
    if(temp->ptr_tcb->timeRemaining == _ptr_tcb->timeRemaining)
    {
      toBeReturned = temp;
      break;      
    }      
  }  
    
  return toBeReturned;
}
                 
/*
************************************************************************************************************************
*                                                       Stack
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void stack_init()
{
  OS_ERR        err;
  
  OSMemCreate(  (OS_MEM         *)&Stack_MEM,
                (CPU_CHAR       *)"StackPartition",
                (void           *)&Stack_STORAGE[0],
                (OS_MEM_QTY      ) STACK_MAXSIZE,
                (OS_MEM_SIZE     ) sizeof(OS_MUTEX*),
                (OS_ERR         *)&err);
  
  for (int i = 0; i<STACK_MAXSIZE; i++)
  {
    Stack_STORAGE[i] = (OS_MUTEX *)0;
  }
}

int isempty() 
{
   if(top == -1)
      return 1;
   else
      return 0;
}

int isfull() 
{
   if(top == STACK_MAXSIZE)
      return 1;
   else
      return 0;
}

OS_MUTEX* pop() {
   OS_MUTEX *data;
   if(!isempty()) {
      data = Stack_STORAGE[top];
      top = top - 1;   
      return data;
   } else {
      //printf("Could not retrieve data, Stack is empty.\n");
      return 0;
   }
}

void push(OS_MUTEX *data) {

   if(!isfull()) {
      top = top + 1;   
      Stack_STORAGE[top] = data;
   } else {
      //printf("Could not insert data, Stack is full.\n");
   }
}

int peek() {
   return Stack_STORAGE[top]->resourceCeiling;
}
/*
************************************************************************************************************************
*                                                       AVL Tree
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

// Initialize AVL tree
void AVL_init(){
  OS_ERR      err;
  int           i;
  AVL_node  *node;
  AVL_root = (AVL_node *)0;
  AVL_root->nextptr = (AVL_node *)0;
  AVL_root->left = (AVL_node *)0;
  AVL_root->right = (AVL_node *)0;

    for (i = 0u; i < 50; i++) {                /* Initialize the array of OS_RDY_LIST at each priority   */
        node = &AVL_array[i];
        node->height = 0;
        node->left = (AVL_node *)0;
        node->right = (AVL_node *)0;
        node->tcb_ptr = (OS_TCB *)0;
        node->nextptr = (AVL_node *)0;
    }
    
    OSMemCreate((OS_MEM         *)&AVL_Partition,
                (CPU_CHAR       *)"AVL Partition",
                (void           *)&AVL_array[0],
                (OS_MEM_QTY      ) 50,
                (OS_MEM_SIZE     ) sizeof(AVL_node),
                (OS_ERR         *)&err);
}

// Insert node into the AVL tree, the key is the absolute deadline
void AVL_insertDeadline(OS_TCB *tcb_ptr){
    //printf("Inserted %d\n", tcb_ptr->Prio );
    // Search whether there is any existing same value deadline
    AVL_node  *node = AVL_search(AVL_root, tcb_ptr);
    
    // if cannot find any existing node
    if (node ==  (AVL_node *)0){
      // Insert the TCB into the AVL tree
      AVL_root = AVL_insertNode(AVL_root, tcb_ptr); 
    }
    else { 
      // there is duplicated node, chain the node to the existing node
      AVL_node  *temp = AVL_newNode(tcb_ptr);
      if (node->nextptr == (AVL_node *)0){
        node->nextptr = temp;
        temp->nextptr = (AVL_node *)0;
      }
      else {
        while (node->nextptr != (AVL_node *)0){
          node = node->nextptr;
        }
        node->nextptr = temp;
        temp->nextptr = (AVL_node *)0;
      }
    }
}

// Delete the node from the AVL tree
void AVL_removeDeadline(OS_TCB *tcb_ptr){
  //printf("Removed %d\n", tcb_ptr->Prio );
  AVL_root = AVL_deleteNode(AVL_root, tcb_ptr);
}

// Get the smallest deadline in the AVL tree
AVL_node *AVL_getLowestDeadline(){
   return minDeadline(AVL_root);
}

// Internal use in the AVL tree
CPU_INT08U AVL_max(CPU_INT08U a, CPU_INT08U b){
    return (a > b) ? a : b;
}

// Internal use in the AVL tree
CPU_INT08U AVL_height(AVL_node *node){
    if (node == (AVL_node*)0)
      return 0;
    return node->height;
}

// Creating a new node
AVL_node *AVL_newNode(OS_TCB *tcb_ptr){
    OS_ERR err;
    AVL_node *node = (AVL_node*)OSMemGet((OS_MEM  *)&AVL_Partition, (OS_ERR  *)&err);
    if (err == OS_ERR_NONE){
      node->left = (AVL_node*)0;
      node->right = (AVL_node*)0;
      node->tcb_ptr = tcb_ptr;
      node->height = 0;
      node->nextptr = (AVL_node *)0;
    }
    
    return node;
}

// Internal use in the AVL tree
AVL_node *AVL_rightRotate(AVL_node *node){
    AVL_node *x1 = node->left;
    AVL_node *temp = x1->right;
    
    // Perform rotation
    x1->right = node;
    node->left = temp;
    
    // update heights
    node->height = AVL_max(AVL_height(node->left), AVL_height(node->right)) + 1;
    x1->height = AVL_max(AVL_height(x1->left), AVL_height(x1->right)) + 1;
    
    return x1;
}

// Internal use in the AVL tree
AVL_node *AVL_leftRotate(AVL_node *node){
    AVL_node *y1 = node->right;
    AVL_node *temp = y1->left;
    
    // perform rotation
    y1->left = node;
    node->right = temp;
    
    // update heights
    node->height = AVL_max(AVL_height(node->left), AVL_height(node->right)) + 1;
    y1->height = AVL_max(AVL_height(y1->left), AVL_height(y1->right)) + 1;
    
    return y1;
}

// Internal use in the AVL tree
CPU_INT16S AVL_getBalance(AVL_node *node){
    if (node == (AVL_node *)0)
      return 0;
    return AVL_height(node->left) - AVL_height(node->right);
}

// Internal use in the AVL tree
AVL_node *AVL_insertNode(AVL_node *node, OS_TCB *tcb_ptr){
    // do the Balanced Search Tree rotation
    if (node == (AVL_node*)0)
      return (AVL_newNode(tcb_ptr));
    
    if (tcb_ptr->deadline < node->tcb_ptr->deadline)
      node->left = AVL_insertNode(node->left, tcb_ptr);
    else if (tcb_ptr->deadline > node->tcb_ptr->deadline)
      node->right = AVL_insertNode(node->right, tcb_ptr);
    else
      return node;
    
    // update height of the parent node
    node->height = 1 + AVL_max(AVL_height(node->left),
                               AVL_height(node->right));
    
    // get the balance factor of this ancestor node to check whether this node became unbalanced
    CPU_INT16S balance = AVL_getBalance(node);
    
    // check if this node becomes unbalanced, then there are 4 different cases
    
    // left Left Case
    if (balance > 1 && tcb_ptr->deadline < node->left->tcb_ptr->deadline)
      return AVL_rightRotate(node);
    
    // right Right Case
    if (balance < -1 && tcb_ptr->deadline > node->right->tcb_ptr->deadline)
      return AVL_leftRotate(node);
    
    // left Right Case
    if (balance > 1 && tcb_ptr->deadline > node->left->tcb_ptr->deadline){
      node->left = AVL_leftRotate(node->left);
      return AVL_rightRotate(node);
    }
    
    // right Left Case
    if (balance < -1 && tcb_ptr->deadline < node->right->tcb_ptr->deadline){
      node->right = AVL_rightRotate(node->right);
      return AVL_leftRotate(node);
    }
    
    // unchanged
    return node;
}

// Internal use in the AVL tree
AVL_node *AVL_minValueNode(AVL_node *node, OS_TCB *tcb_ptr){
    AVL_node *current = node;
    
    // tranverse to the left most node
    while (current->left != (AVL_node *)0){
      current = current->left;
    }
    
    return current;
}

// Remove the node from the AVL tree
AVL_node   *AVL_deleteNode(AVL_node *root, OS_TCB *tcb_ptr){
    // do standard BST delete
    if (root == (AVL_node *)0)
      return root;
    
    // if the key to be deleted is smaller than the
    // root's key, then it lies in left subtree
    if (tcb_ptr->deadline < root->tcb_ptr->deadline)
      root->left = AVL_deleteNode(root->left, tcb_ptr);
    
    // if the key to be deleted is greater than the
    // root's key, then it ies in right subtree
    else if (tcb_ptr->deadline > root->tcb_ptr->deadline)
      root->right = AVL_deleteNode(root->right, tcb_ptr);
    // if key is same as root's key, then This is
    // the node to be deleted
    else {
      // node with only one child or no child
      if ((root->left == (AVL_node *)0) || (root->right == (AVL_node *)0)){
        AVL_node * temp = root->left ? root->left : root->right;
        
        // no child case
        if (temp == (AVL_node *)0){
          temp = root;
          root = (AVL_node *)0;
        }
        else // one child case
          *root = *temp; // copy the contents of the non-empty child
        
        // free memory
        OS_ERR err;
        
        temp->height = 0;
        temp->left = (AVL_node *)0;
        temp->right = (AVL_node *)0;
        temp->tcb_ptr = (OS_TCB *)0;
        
        AVL_node * head = temp;
        AVL_node * current = temp->nextptr;
        while (current != (AVL_node *)0){
          temp->nextptr = current->nextptr;
          current->nextptr = (AVL_node *)0;
          OSMemPut((OS_MEM *)&AVL_Partition, (void *)current, (OS_ERR *)&err);
          current = temp->nextptr;
        }
       
        temp->nextptr = (AVL_node *)0;
        OSMemPut((OS_MEM *)&AVL_Partition, (void *)head, (OS_ERR *)&err);
      }
      else {
        // node with two children: Get the inorder
        // successor (smallest in the right subtree)
        AVL_node *temp = AVL_minValueNode(root->right, tcb_ptr);
        
        // copy the inorder successor's data to this node
        root->tcb_ptr = temp->tcb_ptr;
        
        // delete the inorder successor
        root->right = AVL_deleteNode(root->right, temp->tcb_ptr);
      }
    }
    
    // if the tree had only one node then return
    if (root == (AVL_node *)0)
      return root;
    
    // update height of the current node
    root->height = 1 + AVL_max(AVL_height(root->left), AVL_height(root->right));
    
    // get the balance factor of this node (to check whether this node became unbalanced
    CPU_INT16S balance = AVL_getBalance(root);
    
    // if this node becomes unbalanced, then there are 4 cases
    // left Left Case
    if (balance > 1 && AVL_getBalance(root->left) >= 0)
      return AVL_rightRotate(root);
    
    // left Right Case
    if (balance > 1 && AVL_getBalance(root->left) < 0){
      root->left = AVL_leftRotate(root->left);
      return AVL_rightRotate(root);
    }
    
    // right Right Case
    if (balance < -1 && AVL_getBalance(root->right) <= 0){
      return AVL_leftRotate(root);
    }
    
    // right Left Case
    if (balance < -1 && AVL_getBalance(root->right) > 0){
      root->right = AVL_rightRotate(root->right);
      return AVL_leftRotate(root);
    }
    
    // unchanged
    return root;
}

// Search the node based on deadline, return NULL if unable find the node 
AVL_node   *AVL_search(AVL_node *root, OS_TCB *tcb_ptr){
    if (root == (AVL_node *)0)
      return (AVL_node *)0;
    
    if (tcb_ptr->deadline == root->tcb_ptr->deadline)
      return root;
    else if (tcb_ptr->deadline < root->tcb_ptr->deadline)
      AVL_search(root->left, root->tcb_ptr);
    else if (tcb_ptr->deadline > root->tcb_ptr->deadline)
      AVL_search(root->right, root->tcb_ptr);
}

// Get the min deadline node
AVL_node *minDeadline(AVL_node *node){
  AVL_node* current = node;
  
  if (current == (AVL_node *)0){
    return (AVL_node *)0;
  }
  
  /* loop down to find the leftmost leaf */
  while (current->left != (AVL_node *)0) {
    current = current->left;
  }
  
  if (current == (AVL_node *)0){
    return (AVL_node *)0;
  }
  
  return (current);
}

OS_TCB* searchAVL()
{
  AVL_node* temp;
  temp = AVL_getLowestDeadline();
  
  if (temp != (AVL_node *)0)
    return temp->tcb_ptr;
 
  return (OS_TCB *)0;
}



/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/


//Initialize RedBlackTree and Binary Heap
void OSRec_Init(void)
{
  //Initialize
  RedBlackTree_init();
  SkipList_init();
  stack_init();
  AVL_init();
}

//Create recursive Task Node
void  OS_RecTaskCreate (OS_TCB        *_p_tcb,
                    CPU_CHAR      *_p_name,
                    OS_TASK_PTR    _p_task,
                    void          *_p_arg,
                    OS_PRIO        _prio,
                    CPU_STK       *_p_stk_base,
                    CPU_STK_SIZE   _stk_limit,
                    CPU_STK_SIZE   _stk_size,
                    OS_MSG_QTY     _q_size,
                    OS_TICK        _time_quanta,
                    void          *_p_ext,
                    OS_OPT         _opt,
                    OS_ERR        *_p_err,
                    CPU_INT32U     _timeRemaining,
                    CPU_INT32U     _period,
                    CPU_INT32U     _deadline)
{
   OS_ERR err;
   
   Middle_TCB *middle_tcb = (Middle_TCB *)OSMemGet(&OS_Middle_MEM,
                                         &err);
    
   middle_tcb->p_tcb         = _p_tcb;
   middle_tcb->p_name        = _p_name; 
   middle_tcb->p_task        = _p_task;
   middle_tcb->p_arg         = _p_arg;
   middle_tcb->prio          = _prio;
   middle_tcb->p_stk_base    = _p_stk_base;
   middle_tcb->stk_limit     = _stk_limit;
   middle_tcb->stk_size      = _stk_size;
   middle_tcb->q_size        = _q_size;
   middle_tcb->time_quanta   = _time_quanta;
   middle_tcb->p_ext         = _p_ext;
   middle_tcb->opt           = _opt;
   middle_tcb->p_err         = _p_err;
   middle_tcb->timeRemaining = _timeRemaining;
   middle_tcb->period        = _period;
   middle_tcb->deadline      = _deadline;

   RedBlackTree_insert(middle_tcb);
}


//Scheduler Task
void basic(void)
{
  OS_ERR      err;
  CPU_TS      ts;
  OS_SEM_CTR  ctr;
  
  while(1)
  {
    //Waiting for Semaphore
    ctr = OSSemPend(&sema,0,OS_OPT_PEND_BLOCKING,&ts,&err);
    releaseTime++;
    //printf("%d\n" , releaseTime);
    RedBlackTree_updateTime();
  }
}

// Basic Task for while loop
void OS_Recstart(void)
{

    RedBlackNode *root = redBlackTree->rootNode;  
    RedBlackNode *temp = root;
    RedBlackTree_initialRelease(temp);
      
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&BasicTaskTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) basic, 
                 (void       *) 0, 
                 (OS_PRIO     ) BasicTask_PRIO, 
                 (CPU_STK    *) &BasicTaskStk[0], 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
}


//Task Create Called by Binary Tree
void  OS_RecTaskSkipListCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    CPU_INT32U     period,
                    CPU_INT32U     deadline)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
    
    p_tcb->period        = period;
    
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}


/*
************************************************************************************************************************
*                                                 Mutex Create
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/
void  OS_RecMutexCreate (OS_MUTEX    *p_mutex,
                     CPU_CHAR    *p_name,
                     OS_ERR      *p_err,
                     CPU_INT32U  resourceCeiling)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->NamePtr           =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    p_mutex->resourceCeiling   = resourceCeiling;
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */

#if OS_CFG_DBG_EN > 0u
    OS_MutexDbgListAdd(p_mutex);
#endif
    OSMutexQty++;

    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;
}

/*
************************************************************************************************************************
*                                                 Mutex Pend 
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/
                  
void  OS_RecMutexPend (OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err)
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    }

    CPU_CRITICAL_ENTER();
    //OS_ERR err;
    //OSSchedLock(&err); 
    if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) {    /* Resource available?                                    */
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }
        
        /*------------------------- Push to Stack --------------------------- */
        
        push(p_mutex);
    
        /*------------------------- Update System Ceiling ------------------- */
    
        if(systemCeiling > p_mutex->resourceCeiling)
        {
          systemCeiling = p_mutex->resourceCeiling;
        }
        //printf("Pend System Ceiling: %d , %d\n", systemCeiling, p_mutex->OwnerTCBPtr->Prio);
        /*------------------------------------------------------------------- */
        
        CPU_CRITICAL_EXIT();
        //OSSchedUnlock(&err);
        *p_err                     =  OS_ERR_NONE;
        return;
    }
 
    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }

    if ((opt & OS_OPT_PEND_NON_BLOCKING) != (OS_OPT)0) {    /* Caller wants to block if not available?                */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_PEND_WOULD_BLOCK;                   /* No                                                     */
        return;
    } else {
        if (OSSchedLockNestingCtr > (OS_NESTING_CTR)0) {    /* Can't pend when the scheduler is locked                */
            CPU_CRITICAL_EXIT();
            *p_err = OS_ERR_SCHED_LOCKED;
            return;
        }
    }
    
    /* PIP Protocol */
    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();                  /* Lock the scheduler/re-enable interrupts                */
    p_tcb = p_mutex->OwnerTCBPtr;                           /* Point to the TCB of the Mutex owner                    */
    if (p_tcb->Prio > OSTCBCurPtr->Prio) {                  /* See if mutex owner has a lower priority than current   */
        switch (p_tcb->TaskState) {
            case OS_TASK_STATE_RDY:
                 OS_RdyListRemove(p_tcb);                   /* Remove from ready list at current priority             */
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Raise owner's priority                                 */
                 OS_PrioInsert(p_tcb->Prio);
                 OS_RdyListInsertHead(p_tcb);               /* Insert in ready list at new priority                   */
               break;

            case OS_TASK_STATE_DLY:
            case OS_TASK_STATE_DLY_SUSPENDED:
            case OS_TASK_STATE_SUSPENDED:
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Only need to raise the owner's priority                */
                 break;

            case OS_TASK_STATE_PEND:                        /* Change the position of the task in the wait list       */
            case OS_TASK_STATE_PEND_TIMEOUT:
            case OS_TASK_STATE_PEND_SUSPENDED:
            case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
                 OS_PendListChangePrio(p_tcb,
                                       OSTCBCurPtr->Prio);
                 break;

            default:
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_STATE_INVALID;
                 return;
        }
    }

    OS_Pend(&pend_data,                                     /* Block task pending on Mutex                            */
            (OS_PEND_OBJ *)((void *)p_mutex),
             OS_TASK_PEND_ON_MUTEX,
             timeout);

    OS_CRITICAL_EXIT_NO_SCHED();

    //OSSched();                                              /* Find the next highest priority task ready to run       */

    CPU_CRITICAL_ENTER();
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_NONE;
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = (CPU_TS  )0;
             }
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();
}
            
/*
************************************************************************************************************************
*                                                 Mutex Post
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/
void  OS_RecMutexPost (OS_MUTEX  *p_mutex,
                   OS_OPT     opt,
                   OS_ERR    *p_err)

{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    
    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) 
    {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }

/*------------------------- Pop from Stack --------------------------- */
        OS_MUTEX* temp = pop();

        if(!(isempty()))
        {
          int tempRC;
          /*------------------------- Update System Ceiling ------------------- */
          int maxRC = 0;
          for(int i = top; i>-1; i--)
          {
            tempRC = Stack_STORAGE[i]->resourceCeiling;
            if(tempRC > maxRC)
            {
              maxRC = tempRC;
            }
          }
          systemCeiling = maxRC;
        }
        else
        {
            systemCeiling = 99;
        }
        //printf("Post System ceiling: %d\n", systemCeiling);
        /*---- Search AVL Tree ----*/
        OS_TCB* AVLlowest = searchAVL();
    
        /* ---- Check if TCB < System Ceiling --- */
        int checkSC = systemCeiling;
        
        if(AVLlowest->Prio < checkSC)
        {
          /*--- Insert into Skip List---*/
          insert_sl(AVLlowest);
          /*--- Delete from AVL Tree ---*/
          AVL_removeDeadline(AVLlowest);
        }
        
    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) {     /* Are we done with all nestings?                         */
        OS_CRITICAL_EXIT();                                 /* No                                                     */
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }

    p_pend_list = &p_mutex->PendList;
    if (p_pend_list->NbrEntries == (OS_OBJ_QTY)0) {         /* Any task waiting on mutex?                             */
        p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
        p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;
        OS_CRITICAL_EXIT();
        *p_err = OS_ERR_NONE;
        return;
    }
                                                            /* Yes                                                    */
     if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) {
          OS_RdyListRemove(OSTCBCurPtr);
          OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     /* Lower owner's priority back to its original one        */
          OS_PrioInsert(OSTCBCurPtr->Prio);
          OS_RdyListInsertTail(OSTCBCurPtr);                  /* Insert owner in ready list at new priority             */
          OSPrioCur         = OSTCBCurPtr->Prio;
      }
                                                            /* Get TCB from head of pend list                         */
    p_tcb                      = p_pend_list->HeadPtr->TCBPtr;
    p_mutex->OwnerTCBPtr       = p_tcb;                     /* Give mutex to new owner                                */
    p_mutex->OwnerOriginalPrio = p_tcb->Prio;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
                                                            /* Post to mutex                                          */
    OS_Post((OS_PEND_OBJ *)((void *)p_mutex),
            (OS_TCB      *)p_tcb,
            (void        *)0,
            (OS_MSG_SIZE  )0,
            (CPU_TS       )ts);

    OS_CRITICAL_EXIT_NO_SCHED();

    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
        OSSched();                                          /* Run the scheduler                                      */
    }

    *p_err = OS_ERR_NONE;
}