/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include <os.h>
#include <stdio.h>
#include <stdlib.h>
/*
************************************************************************************************************************
*                                                 DATA STRUCTURE
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 RED BLACK TREE
************************************************************************************************************************
*/
typedef struct _Middle_TCB{
   OS_TCB         *p_tcb;
   CPU_CHAR       *p_name;
   OS_TASK_PTR     p_task;
   void           *p_arg;
   OS_PRIO         prio;
   CPU_STK        *p_stk_base;
   CPU_STK_SIZE    stk_limit;
   CPU_STK_SIZE    stk_size;
   OS_MSG_QTY      q_size;
   OS_TICK         time_quanta;
   void            *p_ext;
   OS_OPT          opt;
   OS_ERR          *p_err;
   CPU_INT32U      timeRemaining;
   CPU_INT32U      period;
   CPU_INT32U      deadline;
}Middle_TCB;

typedef struct _redblackNode{
    Middle_TCB* ptr_tcb;
    CPU_INT08U  color;  
    struct _redblackNode *left;
    struct _redblackNode *right;
    struct _redblackNode *parent;   
}RedBlackNode;


typedef struct _redblacktree{
    RedBlackNode *rootNode;
    RedBlackNode *neel;
    CPU_INT08U curNumberOfElements;
    CPU_INT08U maxNumberOfElements;
}RedBlackTree;

/*
************************************************************************************************************************
*                                                 RED BLACK TREE FUNCTION PROTOTYPE
************************************************************************************************************************
*/
  
//Allocate memory block and initialize
void RedBlackTree_init(void);

//Red Black Tree insertion
void RedBlackTree_insert(Middle_TCB *);

//Red Black Tree deletion
void RedBlackTree_delete(Middle_TCB *);

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void);

//Update timeRemaining
void RedBlackTree_updateTime(void);

//Additional function
RedBlackNode *BSTFindSlots(Middle_TCB*);
void RedBlackTree_recursiveUpdate(RedBlackNode *);
void  BSTRecursiveFinding(RedBlackNode *);
void RedBlackTree_LeftRotate(RedBlackNode *);
void RedBlackTree_RightRotate(RedBlackNode *);
void RedBlackTree_InsertFix(RedBlackNode *);
void RedBlackTree_DeleteFix(RedBlackNode *);
RedBlackNode* search(Middle_TCB *);
RedBlackNode* Successor(RedBlackNode*);
RedBlackNode* LeftMost(RedBlackNode*);


/*
************************************************************************************************************************
*                                                 Skip List
************************************************************************************************************************
*/
//Structure for Skip List
typedef struct node{
    OS_TCB* ptr_tcb;
    struct node *up;    
    struct node *down;
    struct node *left;
    struct node *right;
}np;

/*
************************************************************************************************************************
*                                                 Skip List FUNCTION PROTOTYPE
************************************************************************************************************************
*/

int toss_coin();
np* createNode();
void toss_it(np *x);
OS_TCB* search_sl(int item);
void delete_sl(int item);
void insert_sl(OS_TCB *);
void SkipList_init(void);
OS_TCB* searchSkip();

/*
************************************************************************************************************************
*                                                               Stack 
************************************************************************************************************************
*/
typedef struct _stack{
    OS_MUTEX* ptr_tcb;
}stack;
/*
************************************************************************************************************************
*                                                 Stack FUNCTION PROTOTYPE
************************************************************************************************************************
*/

int isempty(); 
int isfull();
OS_MUTEX* pop();
void push(OS_MUTEX *);
void stack_init();
int peek();

/*
***********************************************************************************************************************
                                              AVL 
***********************************************************************************************************************
*/

typedef struct _AVL_node{
  struct _AVL_node *left;
  struct _AVL_node *right;
  struct _AVL_node *nextptr;
  OS_TCB *tcb_ptr;
  CPU_INT08U   height;
} AVL_node;

/*
***********************************************************************************************************************
                                    AVL FUNCTION PROTOTYPE
***********************************************************************************************************************
*/
void            AVL_init();
void            AVL_insertDeadline(OS_TCB *tcb_ptr);
void            AVL_removeDeadline(OS_TCB *tcb_ptr);
AVL_node *      AVL_getLowestDeadline();
CPU_INT08U      AVL_max(CPU_INT08U a, CPU_INT08U b);
CPU_INT08U      AVL_height(AVL_node *node);
AVL_node *      AVL_newNode(OS_TCB *tcb_ptr);
AVL_node *      AVL_rightRotate(AVL_node *node);
AVL_node *      AVL_leftRotate(AVL_node *node);
CPU_INT16S      AVL_getBalance(AVL_node *node);
AVL_node *      AVL_insertNode(AVL_node *node, OS_TCB *tcb_ptr);
AVL_node *      AVL_minValueNode(AVL_node *node, OS_TCB *tcb_ptr);
AVL_node *      AVL_deleteNode(AVL_node *root, OS_TCB *tcb_ptr);
AVL_node *      AVL_search(AVL_node *root, OS_TCB *tcb_ptr);
AVL_node *      minDeadline(AVL_node *node);
OS_TCB *        searchAVL();

/*
************************************************************************************************************************
*                                                 OS_RecInitialise Task API
************************************************************************************************************************
*/
//Initialize RedBlackTree and SkipList
void OS_RecInitialise(void);

void  OS_RecTaskCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    CPU_INT32U     remainingTime,
                    CPU_INT32U     period,
                    CPU_INT32U     deadline);

void  OS_RecTaskSkipListCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
		    CPU_INT32U     period,
                    CPU_INT32U     deadline);


void OS_RecTaskDelete(OS_TCB* ptr_tcb);

/*
***********************************************************************************************************************
                                   Muxtex FUNCTION PROTOTYPE
***********************************************************************************************************************
*/
void  OS_RecMutexCreate(OS_MUTEX        *p_mutex,
                        CPU_CHAR        *p_name,
                        OS_ERR          *p_err,
                        CPU_INT32U      resourceCeiling);

void  OS_RecMutexPend(OS_MUTEX          *p_mutex,
                      OS_TICK           timeout,
                      OS_OPT            opt,
                      CPU_TS            *p_ts,
                      OS_ERR            *p_err);

void  OS_RecMutexPost(OS_MUTEX          *p_mutex,
                      OS_OPT            opt,
                      OS_ERR            *p_err);

                        
//BasicTask Global variable

#define  BasicTask_STK_SIZE                  128u

static  OS_TCB       BasicTaskTCB;
static  CPU_STK      BasicTaskStk[BasicTask_STK_SIZE];
static void          BasicTask(void  *p_arg);

//Start Rec
void OS_RecStart(void);




