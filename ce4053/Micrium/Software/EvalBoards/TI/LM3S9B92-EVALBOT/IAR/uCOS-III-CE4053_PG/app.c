/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "OS_RecTask.h"

/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   10
#define TASK2PERIOD                   20


#define WORKLOAD1                     3
#define WORKLOAD2                     2


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)


/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       BlinkLeftTCB;
static  CPU_STK      BlinkLeftStk[BlinkLeft_STK_SIZE];

static  OS_TCB       BlinkRightTCB;
static  CPU_STK      BlinkRightStk[BlinkRight_STK_SIZE];

static  OS_TCB       BlinkBothTCB;
static  CPU_STK      BlinkBothStk[BlinkBoth_STK_SIZE];

static  OS_TCB       MoveForwardTCB;
static  CPU_STK      MoveForwardStk[MoveForward_STK_SIZE];

static  OS_TCB       MoveBackwardTCB;
static  CPU_STK      MoveBackwardStk[MoveBackward_STK_SIZE];


CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;
CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;
CPU_INT32U      measure=0;


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void        AppRobotMotorDriveSensorEnable    ();
        void        IntWheelSensor                    ();
        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);
static  void        AppTaskOne                   (void  *p_arg);
static  void        AppTaskTwo                   (void  *p_arg);

static  void        BlinkLeft                    (void  *p_arg);
static  void        BlinkRight                   (void  *p_arg);
static  void        BlinkBoth                    (void  *p_arg);
static  void        MoveForward                  (void  *p_arg);
static  void        MoveBackward                 (void  *p_arg);

/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;
    (void)&p_arg;
    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();          
    
    OSRec_Init();
    
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    /* Initialise the 5 Main Tasks to  Deleted State */
    OS_RecTaskCreate((OS_TCB     *)&BlinkLeftTCB,                   //TCB declare in app.c
                 (CPU_CHAR   *)"BlinkLeft", 
                 (OS_TASK_PTR ) BlinkLeft,                      //Function name
                 (void       *) 0, 
                 (OS_PRIO     ) BlinkLeft_PRIO,                 //Declare in app_cfg
                 (CPU_STK    *)&BlinkLeftStk[0],                //Declare in app.c
                 (CPU_STK_SIZE) BlinkLeft_STK_SIZE / 10u,       //Declare in app_cfg
                 (CPU_STK_SIZE) BlinkLeft_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
                 (CPU_INT32U )  5u,
                 (CPU_INT32U )  5u,
                 (CPU_INT32U )  5u);

    OS_RecTaskCreate((OS_TCB     *)&BlinkRightTCB, 
                 (CPU_CHAR   *)"BlinkRight", 
                 (OS_TASK_PTR ) BlinkRight, 
                 (void       *) 0, 
                 (OS_PRIO     ) BlinkRight_PRIO, 
                 (CPU_STK    *)&BlinkRightStk[0], 
                 (CPU_STK_SIZE) BlinkRight_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BlinkRight_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
		 (CPU_INT32U )  20u,
                 (CPU_INT32U )  20u,
                 (CPU_INT32U )  20u);

   OS_RecTaskCreate((OS_TCB     *)&BlinkBothTCB, 
                 (CPU_CHAR   *)"BlinkBoth", 
                 (OS_TASK_PTR ) BlinkBoth, 
                 (void       *) 0, 
                 (OS_PRIO     ) BlinkBoth_PRIO, 
                 (CPU_STK    *)&BlinkBothStk[0], 
                 (CPU_STK_SIZE) BlinkBoth_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BlinkBoth_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
		 (CPU_INT32U )  15u,
                 (CPU_INT32U )  15u,
                 (CPU_INT32U )  15u);
    
     OS_RecTaskCreate((OS_TCB     *)&MoveForwardTCB, 
                 (CPU_CHAR   *)"MoveForward", 
                 (OS_TASK_PTR ) MoveForward, 
                 (void       *) 0, 
                 (OS_PRIO     ) MoveForward_PRIO, 
                 (CPU_STK    *)&MoveForwardStk[0], 
                 (CPU_STK_SIZE) MoveForward_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) MoveForward_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
                 (CPU_INT32U ) 35u,
                 (CPU_INT32U ) 35u,
                 (CPU_INT32U ) 35u);
        
    OS_RecTaskCreate((OS_TCB     *)&MoveBackwardTCB, 
                 (CPU_CHAR   *)"MoveBackward", 
                 (OS_TASK_PTR ) MoveBackward, 
                 (void       *) 0, 
                 (OS_PRIO     ) MoveBackward_PRIO, 
                 (CPU_STK    *)&MoveBackwardStk[0], 
                 (CPU_STK_SIZE) MoveBackward_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) MoveBackward_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
		 (CPU_INT32U )  60u,
                 (CPU_INT32U )  60u,
                 (CPU_INT32U )  60u);      
    
	
   
   
    OS_Recstart();
    
    /* Delete this task */
    OSTaskDel((OS_TCB *)0, &err);
    
}

static void  BlinkLeft (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
    //printf("Start of BlinkLeft\n");
    BSP_LED_Toggle(2u);
	
    for(i=0; i <(ONESECONDTICK/2); i++)
	{
         j = ((i * 2)+j);
    }
	
    BSP_LED_Off(2u);
    //printf("End of BlinkLeft\n");
    OSTaskDel(&BlinkLeftTCB, &err);  
}

static void  BlinkRight (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
    //printf("Start of BlinkRight\n");
    BSP_LED_Toggle(1u);
	
    for(i=0; i <(ONESECONDTICK/2); i++)
	{
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(1u);
    //printf("End of BlinkRight\n");
    OSTaskDel(&BlinkRightTCB, &err);
}

static void  BlinkBoth (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
    //printf("Start of BlinkBoth\n");
    BSP_LED_Toggle(0u);
	
    for(i=0; i <(ONESECONDTICK/2); i++)
	{
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(0u);
    //printf("End of BlinkBoth\n");
    OSTaskDel(&BlinkBothTCB, &err);
    
}

static void  MoveForward (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    iMove = 10;
    //printf("Start of MoveForward\n");
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
      RoboTurn(FRONT, 16, 50);
      iMove--;
      }
    }
    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    //printf("End of MoveForward\n");
    OSTaskDel(&MoveForwardTCB, &err);
}

static void  MoveBackward (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    iMove = 10;
    
   //printf("Start of MoveBackward\n");
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
        RoboTurn(BACK, 16, 50);
        iMove--;
      }
    }
    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    //printf("End of MoveBackward\n");
    OSTaskDel(&MoveBackwardTCB, &err); 
}


static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}
