/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/
#include "OS_RecTask.h"

/*
************************************************************************************************************************
*                                                 GLOBAL VARIABLE
************************************************************************************************************************
*/


//Global Variable
np *list;
int height=1,width=1;
np *ln;

int releaseTime = 0;

//Red Black Tree
//Red Black Tree Node
OS_MEM            redblacktreeNodeMEM;
RedBlackNode      alloMemredblackNode[40];   //1 row for 40 words of RedBlackNode

//Red Black Tree
OS_MEM            redblackTreeMEM;
RedBlackTree      alloMemredblackTree[1];    //1 row for 1 word of RedBlackTree

//Red Black recursive List reference
RedBlackTree *redBlackTree = (RedBlackTree *)0;


//Middle_TCB
OS_MEM            OS_Middle_MEM;
Middle_TCB       alloMemOS_Middle_TCB[40];


//Skip List
OS_MEM            OS_TCB_MEM;
OS_TCB            OS_TCB_STORAGE[50];

//SkipList Mem Block
OS_MEM            SkipList_MEM;
np                SkipList_STORAGE[50];


/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 Skip List
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void SkipList_init(void)
{
  OS_ERR        err;
  
  OSMemCreate(  (OS_MEM         *)&SkipList_MEM,
                (CPU_CHAR       *)"SkipListPartition",
                (void           *)&SkipList_STORAGE[0],
                (OS_MEM_QTY      ) 50,
                (OS_MEM_SIZE     ) sizeof(np),
                (OS_ERR         *)&err);
  
  OSMemCreate(  (OS_MEM         *)&OS_TCB_MEM,
                (CPU_CHAR       *)"OS_TCBPartition",
                (void           *)&OS_TCB_STORAGE[0],
                (OS_MEM_QTY      ) 50,
                (OS_MEM_SIZE     ) sizeof(OS_TCB),
                (OS_ERR         *)&err);
  
}

// ******************** Testing for skip list ************* //
void insert_sl(OS_TCB *item){
    if(item<=0)return ;
    if(!list){//for the first item
        list=createNode();
        np *newnode=createNode();
        list->right=newnode;
        newnode->left=list;
        newnode->ptr_tcb = item;
        //toss to go upper level
        toss_it(list->right);
        width++;
        
        return;
    }
    //if list is not empty, find the right position
    np *curr=list,*temp;
    while(curr!=(np*)0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item->deadline){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item->deadline)return;
        else {
            curr=curr->down;
            
        }
    }
     
    np *newnode=createNode();
    newnode->ptr_tcb =item;
    if(temp->right==(np*)0){//when added at the right most
        temp->right=newnode;
        newnode->left=temp;
    }
    else{//when added between two nodes
        newnode->left=temp;
        newnode->right=temp->right;
        temp->right->left=newnode;
        temp->right=newnode;      
    }      
    toss_it(newnode);
    width++;
    return ;  
}


// ******************** Testing for skip list ************* //
OS_TCB* search_sl(int item){
    np *curr=list,*temp;
    while(curr!=0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item)
          return curr->right->ptr_tcb;
        else {
            curr=curr->down;
            
        }
    }
      return 0;  
}



// ******************** Testing for skip list ************* //
void delete_sl(int item){
    np *curr=list,*temp;
    int down_count=0;
    while(curr!=(np*)0){
        temp=curr;
        if(curr->right && curr->right->ptr_tcb->deadline<item){
            curr=curr->right;
            
        }
        else if(curr->right && curr->right->ptr_tcb->deadline==item){
            np *nodeTemp=curr->right;
            while(nodeTemp){//go down one by one
                if(nodeTemp->left==((np*)0) && nodeTemp->right==((np*)0)){
                    down_count++;//count the level from uppermost level      
                }
                if(nodeTemp->right){
                    nodeTemp->right->left=nodeTemp->left;
                    nodeTemp->left->right=nodeTemp->right;              
                }
                else{
                    nodeTemp->left->right=(np*)0;
                }
                np *nd=nodeTemp->down;
                
                OS_ERR err;

                OSMemPut( (OS_MEM *)&SkipList_MEM,
                          (void   *)nodeTemp,
                          (OS_ERR *) &err);
                
                nodeTemp=nd;          
            }
            //update the width and height
            width--;
            height=height-down_count;
            return ;
        }
        else {
            curr=curr->down;
         
        }
    }
    return ;
      
}


// ******************** Testing for skip list ************* //
void toss_it(np *x){
    int h=1;
    while(toss_coin()){
        //printf("\nToss Win");
        h++;
        if(h>height){//create a new level
            height=h;
            ln=createNode();
            ln->down=list;
            list->up=ln;
            list=ln;
            //add the node to the new level
            np *newnode=createNode();
            ln->right=newnode;
            newnode->ptr_tcb=x->ptr_tcb;
            newnode->down=x;
            newnode->left=ln;
            x->up=newnode;
            x=newnode;
        }
        else{//add the node to an existing level      
            np *temp=x->left;
            while(temp->up==(np*)0){
                temp=temp->left;
            }
            temp=temp->up;
            np *newnode=createNode();
            newnode->ptr_tcb=x->ptr_tcb;
            newnode->left=temp;
            newnode->down=x;
            temp->right=newnode;
            x->up=newnode;
            x=newnode;
        }  
    }  
}


// ******************** Testing for skip list ************* //
int toss_coin(){
    float t=(float)(rand()%100)/100;
    return t>0.5?1:0;
}


// ******************** Testing for skip list ************* //
np *createNode(){
    OS_ERR      err;
    
    np *newnode =      (np *)OSMemGet((OS_MEM *)&SkipList_MEM,
                                      (OS_ERR *)&err);
    
    newnode->ptr_tcb = (OS_TCB*)OSMemGet((OS_MEM *)&OS_TCB_MEM,
                                         (OS_ERR *)&err);
    
    newnode->ptr_tcb->deadline = -1;
    
    newnode->left = (np*)0;
    
    newnode->down = (np*)0;
    
    newnode->up = (np*)0;
    
    newnode->right = (np*)0;
    
    return newnode;
}


// ******************** Testing for skip list ************* //
OS_TCB* searchSkip()
{
  OS_TCB* temp;
  for (int i = 0; i<5; i++)
  {
    switch(i)
    {
      case 0: temp = search_sl(5+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 1: temp = search_sl(15+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 2: temp = search_sl(20+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 3: temp = search_sl(35+releaseTime);
              if(temp!= 0)
              {return temp;}
      case 4: temp = search_sl(60+releaseTime);
              if(temp!= 0)
              {return temp;}
    }
  }
  return 0;
}


/*
************************************************************************************************************************
*                                                 RED BLACK TREE
************************************************************************************************************************
*/
/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/

void RedBlackTree_init(void)
{
    //Allocate Memory for Binary Nodes
    
    OS_ERR err;
     
    //Allocate Memory to Red Black Tree Node
    OSMemCreate((OS_MEM         *)&redblacktreeNodeMEM,
                (CPU_CHAR       *)"RedBlackTreeNodePartition",
                (void           *)&alloMemredblackNode[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(RedBlackNode),
                (OS_ERR         *)&err);
    
    
    //Allocate Memory to Red Black Tree
    OSMemCreate((OS_MEM         *)&redblackTreeMEM,
                (CPU_CHAR       *)"RedBlackTreePartition",
                (void           *)&alloMemredblackTree[0],
                (OS_MEM_QTY      ) 2,
                (OS_MEM_SIZE     ) sizeof(RedBlackTree),
                (OS_ERR         *)&err);
    
     //Allocate Memory to Middle_TCB
    OSMemCreate((OS_MEM         *)&OS_Middle_MEM,
                (CPU_CHAR       *)"Middle_TCB",
                (void           *)&alloMemOS_Middle_TCB[0],
                (OS_MEM_QTY      ) 40,
                (OS_MEM_SIZE     ) sizeof(Middle_TCB),
                (OS_ERR         *)&err);    
    
    
    //Initialize nodelists
    redBlackTree = (RedBlackTree*)OSMemGet(
                                      (OS_MEM  *)&redblackTreeMEM, 
                                      (OS_ERR  *)&err);
    
    redBlackTree->rootNode = (RedBlackNode*)0;
    redBlackTree->neel =  (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    redBlackTree->neel->color = 1       ;
    
}

//BST insertion
void RedBlackTree_insert(Middle_TCB * _ptr_tcb)
{
  OS_ERR err;
  
  RedBlackNode *y = redBlackTree->neel;
  RedBlackNode *x = redBlackTree->rootNode;
  RedBlackNode *newNode = (RedBlackNode *)OSMemGet(&redblacktreeNodeMEM,
                                         &err);
    
  newNode->ptr_tcb = _ptr_tcb;
  
  if(redBlackTree->rootNode != (RedBlackNode*)0)
  {
    while(x != redBlackTree->neel)
    {
      y = x;
      if(newNode->ptr_tcb->timeRemaining < x->ptr_tcb->timeRemaining)
      {
        x = x->left;
      }
      else
      {
        x = x->right;      
      }
    }
  }
  newNode->parent = y;
  
  if(y == redBlackTree->neel)
  {
    redBlackTree->rootNode = newNode;    
  }
  else if(newNode->ptr_tcb->timeRemaining < y->ptr_tcb->timeRemaining)
  {
    y->left = newNode;    
  }
  else
  {
    y->right = newNode;
  }
  
  newNode->left = redBlackTree->neel;
  newNode->right = redBlackTree->neel;
  newNode->color = (CPU_INT08U)0;
    
  RedBlackTree_InsertFix(newNode);
} 

//Red Black Tree deletion
void RedBlackTree_delete(Middle_TCB *_ptr_tcb)
{
 
  OS_ERR err;
  
  //Check if the node is exist
  RedBlackNode *delete_node = search(_ptr_tcb);
  
  if(delete_node == (RedBlackNode*)0)
  {
    return;
  }
  
  //To be deleted and return memory 
  RedBlackNode *y = (RedBlackNode*)0;
  //The child to be deleted
  RedBlackNode *x = (RedBlackNode*)0;

  if(delete_node->left == redBlackTree->neel || delete_node->right == redBlackTree->neel)
  {
    y = delete_node;    
  }
  else
  {
    y = Successor(delete_node);
  }

  if(y->left != redBlackTree->neel)
  {
    x = y->left;
  }
  else
  {
    x = y->right;   
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;    
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
   y->parent->right = x;     
  }
  
  if(y != delete_node)
  {
    delete_node->ptr_tcb = y->ptr_tcb;
  }
  
  if(y->color ==1)
  {
    RedBlackTree_DeleteFix(x);
  }
  
  //Returning memory 
  
  OSMemPut((OS_MEM *)&redblacktreeNodeMEM,
           (void   *)y,
           (OS_ERR *) &err);
  
}

//print RedBlackTree in the pre-order manner
void RedBlackTree_printTree(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  
  BSTRecursiveFinding(temp);
}

                 
//Update timeRemaining                 
void RedBlackTree_updateTime(void)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;  
  
  //Init Pointer
  
  RedBlackNode *temp = root;
  
  RedBlackTree_recursiveUpdate(temp);   
}

//Inserting to Skip List
void RedBlackTree_recursiveUpdate(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  curNode->ptr_tcb->timeRemaining--;
  
  if(curNode->ptr_tcb->timeRemaining == (CPU_INT32U)0)
  {         
    OS_ERR err;
    Middle_TCB *amiddle_tcb = curNode->ptr_tcb;
    
    OS_RecTaskSkipListCreate(
                 (OS_TCB     *) amiddle_tcb->p_tcb, 
                 (CPU_CHAR   *) amiddle_tcb->p_name, 
                 (OS_TASK_PTR ) amiddle_tcb->p_task, 
                 (void       *) amiddle_tcb->p_arg, 
                 (OS_PRIO     ) amiddle_tcb->prio, 
                 (CPU_STK    *) amiddle_tcb->p_stk_base, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_limit, 
                 (CPU_STK_SIZE) amiddle_tcb->stk_size, 
                 (OS_MSG_QTY  ) amiddle_tcb->q_size, 
                 (OS_TICK     ) amiddle_tcb->time_quanta, 
                 (void       *) (CPU_INT32U) amiddle_tcb->p_ext, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err,
                 (CPU_INT32U) amiddle_tcb->period,
                 (CPU_INT32U) amiddle_tcb->deadline);  
    
    amiddle_tcb->p_tcb->deadline = releaseTime + amiddle_tcb->p_tcb->period;

    insert_sl(amiddle_tcb->p_tcb); 

    amiddle_tcb->timeRemaining = amiddle_tcb->period;
  }
   
  RedBlackTree_recursiveUpdate(curNode->left);
  
  RedBlackTree_recursiveUpdate(curNode->right);   
  
}

//Additional function

RedBlackNode* LeftMost(RedBlackNode* current)
{
  while(current->left != (RedBlackNode*)0)
  {
    current = current->left; 
  }
  return current;
}

RedBlackNode* Successor(RedBlackNode* current)
{
  if(current->right != (RedBlackNode*)0)
  {
    return LeftMost(current->parent);   
  }
  
  RedBlackNode *newNode = current->parent;
  
  while(newNode != (RedBlackNode*)0 && current == newNode->right)
  {
    current = newNode;
    newNode = newNode->parent;    
  }
  return newNode;
}


//Arrange tree after insertion
void RedBlackTree_InsertFix(RedBlackNode *current)
{
  //case 0: if parent is black, no while loop
  
  while(current->parent->color == 0) //if parent is red, go into loop
  {
    //if parent is grandparent's left child
    
    if(current->parent == current->parent->parent->left)
    {
      RedBlackNode *uncle = current->parent->parent->right;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->right)
        {
          current = current->parent;
          RedBlackTree_LeftRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_RightRotate(current->parent->parent);
      }
    }
    //if parent is grandparent's right child
    else
    {
      RedBlackNode *uncle = current->parent->parent->left;
      
      //case 1
      if(uncle->color == 0)
      {
        current->parent->color = 1;
        uncle->color = 1;
        current->parent->parent->color = 0;
        current = current->parent->parent;        
      }
      //case 2 & 3 : uncle is black
      else
      {
        //case 2
        if(current == current->parent->left)
        {
          current = current->parent;
          RedBlackTree_RightRotate(current);
        }
        //case 3
        current->parent->color = 1;                      //parent -> black
        current->parent->parent->color = 0;              //grandparent -> red
        RedBlackTree_LeftRotate(current->parent->parent);
      }      
    }
  }
  redBlackTree->rootNode->color = 1;
}

//Arrange the tree after deletion
void RedBlackTree_DeleteFix(RedBlackNode *current)
{
  //case 0: if current is red, change it to black 
  //        if current is root, change it to black
  
  while(current != redBlackTree->rootNode && current->color == 1)
  {
    //if current is leftchild
    if(current == current->parent->left)
    {
      RedBlackNode *sibling = current->parent->right;
      
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_LeftRotate(current->parent);
        sibling = current->parent->right;        
      }
      
      //Enter case 2, 3 and 4 where sibling is black
      //case 2: if the two childs of sibling are black
      if(sibling->left->color == 1 && sibling->right->color ==1)
      {
        sibling->color = 0;
        //if current is updated to be root, exit the while loop
        current = current->parent;        
      }
      
      //case 3 and 4: there is only one child of current is black
      else
      {
        //case 3: sibling's right child is black and left child is red
        if(sibling->right->color == 1)
        {
            sibling->left->color = 1;
            sibling->color = 0;
            RedBlackTree_RightRotate(sibling);
            sibling = current->parent->right;
        }
        
        //case 4: sibling's left child is black and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        RedBlackTree_LeftRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }
    }
    //current is right child
    else
    {
      RedBlackNode *sibling = current->parent->left;
      //case 1: if sibling is red
      if(sibling->color == 0)
      {
        sibling->color = 1;
        current->parent->color = 0;
        RedBlackTree_RightRotate(current->parent);
        sibling = current->parent->left;        
      }
      //Enter case 2, 3 and 4 where sibling is black
      else
      {
        //case 3: sibling's left child is black and right child is red
        if(sibling->left->color == 1)
        {
          sibling->right->color = 1;
          sibling->color = 0;
          RedBlackTree_LeftRotate(sibling);
          sibling = current->parent->left;  
        }
        //case 4: sibling's left child is red and right child is black
        sibling->color = current->parent->color;
        current->parent->color = 1;
        sibling->left->color = 1;
        RedBlackTree_RightRotate(current->parent);
        current = redBlackTree->rootNode;       //put current as rootnode and jump out of while loop
      }  
    }    
  }
  current->color = 1; 
}
              

//left rotate about the node
void RedBlackTree_LeftRotate(RedBlackNode *x)
{
   RedBlackNode *y = x->right;
   
   x->right = y->left;
   
   if(y->left != redBlackTree->neel)
   {
     y->left->parent = x;
   }
   
   y->parent = x->parent;
   
   if(x->parent == redBlackTree->neel)
   {
     redBlackTree->rootNode = y;
   }
   else if(x == x->parent->left)
   {
     x->parent->left = y;
   }
   else
   {
     x->parent->right = y;     
   }
   
   y -> left = x;
   x-> parent = y;
}
//right rotate about the node
void RedBlackTree_RightRotate(RedBlackNode *y)
{
  RedBlackNode *x = y->left;
  
  y->left = x->right;
  
  if(x->right != redBlackTree->neel)
  {
    x->right->parent = y;
  }
  
  x->parent = y->parent;
  
  if(y->parent == redBlackTree->neel)
  {
    redBlackTree->rootNode = x;
  }
  else if(y == y->parent->left)
  {
    y->parent->left = x;    
  }
  else
  {
    y->parent->right = x;    
  }
  
  x->right  = y;
  y->parent = x;
}

//Recursively print nodes
void  BSTRecursiveFinding(RedBlackNode *curNode)
{
  if(curNode == redBlackTree->neel)
  {
      return;
  }
  
  printf("%d\n",curNode->ptr_tcb->timeRemaining);
  
  BSTRecursiveFinding(curNode->left);
  
  BSTRecursiveFinding(curNode->right);
}


//Get the avaliable leaf node based on ptr tcb
RedBlackNode *BSTFindSlots(Middle_TCB* _ptr_tcb)
{
  //Get Root Node
  RedBlackNode *root = redBlackTree->rootNode;
  
  RedBlackNode *temp = (RedBlackNode*)0;
  
  //Root is null
  if(root == (RedBlackNode*)0)
  {
    return (RedBlackNode*)0;    
  }
  
  //start from root
  temp = root;
  
  //Break while loop if the current temp is the leaf node
  while( (temp->left  != redBlackTree->neel) &&
         (temp->right != redBlackTree->neel))
  {
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
  }  
  
  return temp;
}


RedBlackNode* search(Middle_TCB *_ptr_tcb)
{
  RedBlackNode *temp = redBlackTree->rootNode;
  
  RedBlackNode *toBeReturned = (RedBlackNode*)0;
  
  while(temp != redBlackTree->neel)
  {
    if(_ptr_tcb->timeRemaining > temp->ptr_tcb->timeRemaining)
    {
      temp = temp->right;      
    }
    else
    {
      temp = temp->left;
    }
    if(temp->ptr_tcb->timeRemaining == _ptr_tcb->timeRemaining)
    {
      toBeReturned = temp;
      break;      
    }      
  }  
    
  return toBeReturned;
}
                 
/*
************************************************************************************************************************
*                                                 Recursive Task API
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                 FUNCTION IMPLEMENTATION
************************************************************************************************************************
*/


//Initialize RedBlackTree and Binary Heap
void OSRec_Init(void)
{
  //Initialize
  RedBlackTree_init();
  
  SkipList_init();
}

//Create recursive Task Node
void  OS_RecTaskCreate (OS_TCB        *_p_tcb,
                    CPU_CHAR      *_p_name,
                    OS_TASK_PTR    _p_task,
                    void          *_p_arg,
                    OS_PRIO        _prio,
                    CPU_STK       *_p_stk_base,
                    CPU_STK_SIZE   _stk_limit,
                    CPU_STK_SIZE   _stk_size,
                    OS_MSG_QTY     _q_size,
                    OS_TICK        _time_quanta,
                    void          *_p_ext,
                    OS_OPT         _opt,
                    OS_ERR        *_p_err,
                    CPU_INT32U     _timeRemaining,
                    CPU_INT32U     _period,
                    CPU_INT32U     _deadline)
{
   OS_ERR err;
   
   Middle_TCB *middle_tcb = (Middle_TCB *)OSMemGet(&OS_Middle_MEM,
                                         &err);
    
   middle_tcb->p_tcb         = _p_tcb;
   middle_tcb->p_name        = _p_name; 
   middle_tcb->p_task        = _p_task;
   middle_tcb->p_arg         = _p_arg;
   middle_tcb->prio          = _prio;
   middle_tcb->p_stk_base    = _p_stk_base;
   middle_tcb->stk_limit     = _stk_limit;
   middle_tcb->stk_size      = _stk_size;
   middle_tcb->q_size        = _q_size;
   middle_tcb->time_quanta   = _time_quanta;
   middle_tcb->p_ext         = _p_ext;
   middle_tcb->opt           = _opt;
   middle_tcb->p_err         = _p_err;
   middle_tcb->timeRemaining = _timeRemaining;
   middle_tcb->period        = _period;
   middle_tcb->deadline      = _deadline;

   RedBlackTree_insert(middle_tcb);
}


//Scheduler Task
void basic(void)
{
  OS_ERR      err;
  CPU_TS      ts;
  OS_SEM_CTR  ctr;
  
  while(1)
  {
    //Waiting for Semaphore
    ctr = OSSemPend(&sema,0,OS_OPT_PEND_BLOCKING,&ts,&err);
    releaseTime++;
    //printf("%d\n" , releaseTime);
    RedBlackTree_updateTime();
  }
}

// Basic Task for while loop
void OS_Recstart(void)
{
    OS_ERR err;
    OSTaskCreate(
                 (OS_TCB     *)&BasicTaskTCB, 
                 (CPU_CHAR   *)"TaskScheduler", 
                 (OS_TASK_PTR ) basic, 
                 (void       *) 0, 
                 (OS_PRIO     ) BasicTask_PRIO, 
                 (CPU_STK    *) &BasicTaskStk[0], 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) BasicTask_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *) (CPU_INT32U) 2, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err
    );   
}


//Task Create Called by Binary Tree
void  OS_RecTaskSkipListCreate (OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    CPU_INT32U     period,
                    CPU_INT32U     deadline)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
    
    p_tcb->period        = period;
    
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED(); 
}

